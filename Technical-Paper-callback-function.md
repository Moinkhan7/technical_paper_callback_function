# Technical Paper-Callback Function

## What is Callback Function
A Callback Function is function ( it can be any function Anonymous Function, Arrow Function ) passed into another function as an argument, which is then invoked inside the outer function to complete some kind of routine or action.

### 
```javascript
    function show(){
       console.log("I am show Function");
    }
    function greet(callback){
        callback();
    }
    greet(show);
```
## Types of callback function
 - Synchronous callback function
-  Asynchronous callback function

### 1. Synchronous callback function
Synchronous callback function it waits for each opration to complete, after that it executes the next opration

```javascript
    function show(a){
       console.log("I am show Function " + a);
    }
    function greet(callback){
        let a = "Using in Callback function"
        callback(a);
    }
    greet(show);
    console.log("End")

```
![](https://imgs.developpaper.com/imgs/1908617955-5c5c2a730b8ca_articlex.png)


### 2. Asynchronous callback function
Asynchronous callback function it naver waits for each opration to complete, rather it executes all opration in the first GO only.

```javascript

    setTimeout(function show(){
        console.log("i a show function")
    },5000)

    console.log("End")
```
## Callback hell
Callback hell is a phenomenon that afflicts a JavaScript developer when he tries to execute multiple asynchronous operations one after the other. By nesting callbacks in such a way, we easily end up with error-prone, hard to read, and hard to maintain code.

```javascript 
getRollNo = ()=>{
    setTimeout(()=>{
        console.log('API Getting roll no.');
        let roll_no = [1,2,3,4,5];
        console.log(roll_no);

        setTimeout((rollno)=>{
            const bioData = {
                name : 'Ron',
                age : 26
            }
            console.log(`my name is ${bioData.name} and i am ${bioData.age} year old`)
            setTimeout(()=>{
                bioData.gender = 'male'
                 console.log(`my name is ${bioData.name} and i am ${bioData.age} year old and i am ${bioData.gender}`)
            },2000,bioData.name)
        },2000,roll_no[1])
    },2000);
}
getRollNo();
```

## References

 - https://developpaper.com/javascript-asynchronous-programming-callback-promise-generator/
 - https://www.freecodecamp.org/news/javascript-callback-function-plain-english/
 - https://developer.mozilla.org/en-US/docs/Glossary/Callback_function

